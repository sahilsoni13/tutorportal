<?php

include_once "db_connect.php";
$sql = " SELECT students.`gender`, students.`first_name`, students.`last_name`, students.`city`, students.`contact_number`, jobs.`heading`, jobs.`description`, levels.`level` FROM jobs
LEFT JOIN levels ON levels.`id` = jobs.`level_id` LEFT JOIN students ON students.`id` = jobs.`student_id` ";

if($_GET['search_keyword'] || $_GET['autocomplete']) {
//    print_r($_GET['search_keyword']);
    $sql .= " WHERE 
                   (
                    jobs.`heading` LIKE '%" .$_GET['search_keyword']. "%' 
                   OR jobs.`description` LIKE '%" .$_GET['search_keyword']. "%' ) 
                   
                   AND 
                    ( students.`address_line1` LIKE '%" .$_GET['autocomplete']. "%' 
                   OR students.`address_line2` LIKE '%" .$_GET['autocomplete']. "%' 
                   OR students.`city` LIKE '%" .$_GET['autocomplete']. "%' 
                   OR students.`state` LIKE '%" .$_GET['autocomplete']. "%'
                   OR students.`country` LIKE '%" .$_GET['autocomplete']. "%'
                   OR students.`pincode` LIKE '%" .$_GET['autocomplete']. "%'
                   )
            ";
}
//die($sql);
$jobs = $conn->query($sql);
$jobList = $jobs->num_rows;

//exit;
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Tutor Finder</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css?family=Amatic+SC:400,700|Work+Sans:300,400,700" rel="stylesheet">
    <link rel="stylesheet" href="fonts/icomoon/style.css">

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/magnific-popup.css">
    <link rel="stylesheet" href="css/jquery-ui.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <link rel="stylesheet" href="css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="css/animate.css">
    
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/mediaelement@4.2.7/build/mediaelementplayer.min.css">
    
    
    
    <link rel="stylesheet" href="fonts/flaticon/font/flaticon.css">
  
    <link rel="stylesheet" href="css/aos.css">

    <link rel="stylesheet" href="css/style.css">
    
  </head>
  <body>
  
  <div class="site-wrap">

    <div class="site-mobile-menu">
      <div class="site-mobile-menu-header">
        <div class="site-mobile-menu-close mt-3">
          <span class="icon-close2 js-menu-toggle"></span>
        </div>
      </div>
      <div class="site-mobile-menu-body"></div>
    </div> <!-- .site-mobile-menu -->
    
    
    <div class="site-navbar-wrap js-site-navbar bg-white">
      
      <div class="container">
        <div class="site-navbar bg-light">
          <div class="py-1">
            <div class="row align-items-center">
              <div class="col-2">
                <h2 class="mb-0 site-logo"><a href="index.php">Tutor<strong class="font-weight-bold">Finder</strong> </a></h2>
              </div>
              <div class="col-10">
                <nav class="site-navigation text-right" role="navigation">
                  <div class="container">
                    <div class="d-inline-block d-lg-none ml-md-0 mr-auto py-3"><a href="#" class="site-menu-toggle js-menu-toggle text-black"><span class="icon-menu h3"></span></a></div>

                    <ul class="site-menu js-clone-nav d-none d-lg-block">
                      <li><a href="categories.php">For Candidates</a></li>
                      <li class="has-children">
                        <a href="category.html">For Employees</a>
                        <ul class="dropdown arrow-top">
                          <li><a href="category.html">Category</a></li>
                          <li><a href="#">Browse Candidates</a></li>
                          <li><a href="new-post.php">Post a Job</a></li>
                          <li><a href="#">Employeer Profile</a></li>
                          <li class="has-children">
                            <a href="#">More Links</a>
                            <ul class="dropdown">
                              <li><a href="#">Browse Candidates</a></li>
                              <li><a href="#">Post a Job</a></li>
                              <li><a href="#">Employeer Profile</a></li>
                            </ul>
                          </li>

                        </ul>
                      </li>
                      <li><a href="contact.php">Contact</a></li>
                      <li><a href="new-post.php"><span class="bg-primary text-white py-3 px-4 rounded"><span class="icon-plus mr-3"></span>Post New Job</span></a></li>
                    </ul>
                  </div>
                </nav>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  
    <div style="height: 113px;"></div>

    <div class="site-blocks-cover overlay" style="background-image: url('images/hero_1.jpg');" data-aos="fade" data-stellar-background-ratio="0.5">
      <div class="container">
        <div class="row align-items-center">
          <div class="col-12" data-aos="fade">
            <h1>Find Job</h1>
            <form action="#">
              <div class="row mb-3">
                <div class="col-md-9">
                  <div class="row">
                    <div class="col-md-6 mb-3 mb-md-0">
                      <input type="text" class="mr-3 form-control border-0 px-4" name="search_keyword" id="search_keyword" placeholder="subject/skill, keywords">
                    </div>
                    <div class="col-md-6 mb-3 mb-md-0">
                      <div class="input-wrap">
                        <span class="icon icon-room"></span>
                      <input type="text" class="form-control form-control-block search-input  border-0 px-4" name="autocomplete"  id="autocomplete" placeholder="area, city, state, country or pin code" onFocus="geolocate()">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-3">
                  <input type="submit" class="btn btn-search btn-primary btn-block" value="Search">
                </div>
              </div>
<!--              <div class="row">-->
<!--                <div class="col-md-12">-->
<!--                  <p class="small">or browse by category: <a href="#" class="category">Category #1</a> <a href="#" class="category">Category #2</a></p>-->
<!--                </div>-->
<!--              </div>-->
              
            </form>
          </div>
        </div>
      </div>
    </div>

    <div class="site-section bg-light">
      <div class="container">
        <div class="row">
          <div class="col-md-8 mb-5 mb-md-0" data-aos="fade-up" data-aos-delay="100">
            <h2 class="mb-5 h3">Recent Jobs</h2>
            <div class="rounded border jobs-wrap">


                <?php
                if ($jobList > 0) {
                    while($row = $jobs->fetch_assoc()) {?>



              <a href="job-single.php" class="job-item d-block d-md-flex align-items-center  border-bottom fulltime">
                <div class="company-logo blank-logo text-center text-md-left pl-3">
                  <img src="images/company_logo_blank.png" alt="Image" class="img-fluid mx-auto">
                </div>
                <div class="job-details h-100">
                  <div class="p-3 align-self-center">
                    <h4><?php echo ($row["gender"] == 'Male' ? 'Mr ' : 'Miss ' ).$row["first_name"].' '.$row["last_name"] ?> &mdash; <?php echo $row["heading"] ?></h4>
                    <h3><?php echo $row["description"] ?></h3>
                    <div class="d-block d-lg-flex">
                      <div class="mr-3"><span class="icon-suitcase mr-1"></span> <?php echo $row["level"] ?></div>
                      <div class="mr-3"><span class="icon-room mr-1"></span> <?php echo $row["city"] ?></div>
<!--                      <div><span class="icon-money mr-1"></span> $55000 &mdash; 70000</div>-->
                    </div>
                  </div>
                </div>
                <div class="job-category align-self-center">
                  <div class="p-3">
                    <span class="text-info p-2 rounded border border-info">Full Time</span>
                  </div>
                </div>  
              </a>
                <?php } ?>
                <?php }?>

<!--              <a href="job-single.php" class="job-item d-block d-md-flex align-items-center freelance">-->
<!--                <div class="company-logo blank-logo text-center text-md-left pl-3">-->
<!--                  <img src="images/logo_1.png" alt="Image" class="img-fluid mx-auto">-->
<!--                </div>-->
<!--                <div class="job-details h-100">-->
<!--                  <div class="p-3 align-self-center">-->
<!--                    <h3>JavaScript Fullstack Developer</h3>-->
<!--                    <div class="d-block d-lg-flex">-->
<!--                      <div class="mr-3"><span class="icon-suitcase mr-1"></span> Cooper</div>-->
<!--                      <div class="mr-3"><span class="icon-room mr-1"></span> Anywhere</div>-->
<!--                      <div><span class="icon-money mr-1"></span> $55000 &mdash; 70000</div>-->
<!--                    </div>-->
<!--                  </div>-->
<!--                </div>-->
<!--                <div class="job-category align-self-center">-->
<!--                  <div class="p-3">-->
<!--                    <span class="text-warning p-2 rounded border border-warning">Freelance</span>-->
<!--                  </div>-->
<!--                </div>  -->
<!--              </a>-->
<!---->
<!---->
<!--              <a href="job-single.php" class="job-item d-block d-md-flex align-items-center freelance">-->
<!--                <div class="company-logo blank-logo text-center text-md-left pl-3">-->
<!--                  <img src="images/logo_1.png" alt="Image" class="img-fluid mx-auto">-->
<!--                </div>-->
<!--                <div class="job-details h-100">-->
<!--                  <div class="p-3 align-self-center">-->
<!--                    <h3>ReactJS Fullstack Developer</h3>-->
<!--                    <div class="d-block d-lg-flex">-->
<!--                      <div class="mr-3"><span class="icon-suitcase mr-1"></span> Cooper</div>-->
<!--                      <div class="mr-3"><span class="icon-room mr-1"></span> Anywhere</div>-->
<!--                      <div><span class="icon-money mr-1"></span> $55000 &mdash; 70000</div>-->
<!--                    </div>-->
<!--                  </div>-->
<!--                </div>-->
<!--                <div class="job-category align-self-center">-->
<!--                  <div class="p-3">-->
<!--                    <span class="text-warning p-2 rounded border border-warning">Freelance</span>-->
<!--                  </div>-->
<!--                </div>  -->
<!--              </a>-->
<!---->
<!---->
<!--              <a href="job-single.php" class="job-item d-block d-md-flex align-items-center fulltime">-->
<!--                <div class="company-logo blank-logo text-center text-md-left pl-3">-->
<!--                  <img src="images/company_logo_blank.png" alt="Image" class="img-fluid mx-auto">-->
<!--                </div>-->
<!--                <div class="job-details h-100">-->
<!--                  <div class="p-3 align-self-center">-->
<!--                    <h3>Assistant Brooker, Real Estate</h3>-->
<!--                    <div class="d-block d-lg-flex">-->
<!--                      <div class="mr-3"><span class="icon-suitcase mr-1"></span> RealState</div>-->
<!--                      <div class="mr-3"><span class="icon-room mr-1"></span> New York</div>-->
<!--                      <div><span class="icon-money mr-1"></span> $55000 &mdash; 70000</div>-->
<!--                    </div>-->
<!--                  </div>-->
<!--                </div>-->
<!--                <div class="job-category align-self-center">-->
<!--                  <div class="p-3">-->
<!--                    <span class="text-info p-2 rounded border border-info">Full Time</span>-->
<!--                  </div>-->
<!--                </div>  -->
<!--              </a>-->
<!---->
<!--              <a href="job-single.php" class="job-item d-block d-md-flex align-items-center partime">-->
<!--                <div class="company-logo blank-logo text-center text-md-left pl-3">-->
<!--                  <img src="images/logo_2.png" alt="Image" class="img-fluid mx-auto">-->
<!--                </div>-->
<!--                <div class="job-details h-100">-->
<!--                  <div class="p-3 align-self-center">-->
<!--                    <h3>Telecommunication Manager</h3>-->
<!--                    <div class="d-block d-lg-flex">-->
<!--                      <div class="mr-3"><span class="icon-suitcase mr-1"></span> Think</div>-->
<!--                      <div class="mr-3"><span class="icon-room mr-1"></span> London</div>-->
<!--                    </div>-->
<!--                  </div>-->
<!--                </div>-->
<!--                <div class="job-category align-self-center">-->
<!--                  <div class="p-3">-->
<!--                    <span class="text-danger p-2 rounded border border-danger">Par Time</span>-->
<!--                  </div>-->
<!--                </div>  -->
<!--              </a>-->


            </div>

<!--            <div class="col-md-12 text-center mt-5">-->
<!--              <a href="#" class="btn btn-primary rounded py-3 px-5"><span class="icon-plus-circle"></span> Show More Jobs</a>-->
<!--            </div>-->
          </div>

        </div>
      </div>
    </div>

  </div>

  <script src="js/jquery-3.3.1.min.js"></script>
  <script src="js/jquery-migrate-3.0.1.min.js"></script>
  <script src="js/jquery-ui.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/owl.carousel.min.js"></script>
  <script src="js/jquery.stellar.min.js"></script>
  <script src="js/jquery.countdown.min.js"></script>
  <script src="js/jquery.magnific-popup.min.js"></script>
  <script src="js/bootstrap-datepicker.min.js"></script>
  <script src="js/aos.js"></script>

  
  <script src="js/mediaelement-and-player.min.js"></script>

  <script src="js/main.js"></script>
    

  <script>

//      function submit() {
//          $("form").submit(function(e) {
//              e.preventDefault();
//              $.ajax({
//                  type: 'POST',
//                  url: 'index.php',
//                  data: $('form').serialize(),
//                  success: function() {
//                      alert("Request saved successfully");
//                      $('#frmFindJob').trigger("reset");
//                  },
//                  error: function() {
//                      alert("Something went wrong. Please try again.");
//                  }
//              });
//          });
//      }

      document.addEventListener('DOMContentLoaded', function() {
                var mediaElements = document.querySelectorAll('video, audio'), total = mediaElements.length;

                for (var i = 0; i < total; i++) {
                    new MediaElementPlayer(mediaElements[i], {
                        pluginPath: 'https://cdn.jsdelivr.net/npm/mediaelement@4.2.7/build/',
                        shimScriptAccess: 'always',
                        success: function () {
                            var target = document.body.querySelectorAll('.player'), targetTotal = target.length;
                            for (var j = 0; j < targetTotal; j++) {
                                target[j].style.visibility = 'visible';
                            }
                  }
                });
                }
            });
    </script>


    <script>
      // This example displays an address form, using the autocomplete feature
      // of the Google Places API to help users fill in the information.

      // This example requires the Places library. Include the libraries=places
      // parameter when you first load the API. For example:
      // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

      var placeSearch, autocomplete;
      var componentForm = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        country: 'long_name',
        postal_code: 'short_name'
      };

      function initAutocomplete() {
        // Create the autocomplete object, restricting the search to geographical
        // location types.
        autocomplete = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
            {types: ['geocode']});

        // When the user selects an address from the dropdown, populate the address
        // fields in the form.
        autocomplete.addListener('place_changed', fillInAddress);
      }

      function fillInAddress() {
        // Get the place details from the autocomplete object.
        var place = autocomplete.getPlace();

        for (var component in componentForm) {
          document.getElementById(component).value = '';
          document.getElementById(component).disabled = false;
        }

        // Get each component of the address from the place details
        // and fill the corresponding field on the form.
        for (var i = 0; i < place.address_components.length; i++) {
          var addressType = place.address_components[i].types[0];
          if (componentForm[addressType]) {
            var val = place.address_components[i][componentForm[addressType]];
            document.getElementById(addressType).value = val;
          }
        }
      }

      // Bias the autocomplete object to the user's geographical location,
      // as supplied by the browser's 'navigator.geolocation' object.
      function geolocate() {
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var geolocation = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };
            var circle = new google.maps.Circle({
              center: geolocation,
              radius: position.coords.accuracy
            });
            autocomplete.setBounds(circle.getBounds());
          });
        }
      }
    </script>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&libraries=places&callback=initAutocomplete"
        async defer></script>

  </body>
</html>